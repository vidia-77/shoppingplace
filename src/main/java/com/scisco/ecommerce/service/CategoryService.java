package com.scisco.ecommerce.service;

public interface CategoryService {
    long getGategoryCount();
}
