package com.scisco.ecommerce.service.serviceImpl;

import com.scisco.ecommerce.dto.ArticlesDto;
import com.scisco.ecommerce.model.*;
import com.scisco.ecommerce.repository.ArticlesRepository;
import com.scisco.ecommerce.repository.CotegoriesRepository;
import com.scisco.ecommerce.repository.TailleRepository;
import com.scisco.ecommerce.repository.TypeArticleRepository;
import com.scisco.ecommerce.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

@Service
//@Transactional
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticlesRepository articlesRepository;

    @Autowired
    private TypeArticleRepository typeArticleRepository;

    @Autowired
    private TailleRepository tailleRepository;

    @Autowired
    private CotegoriesRepository categoriesRepository;


    @Override
    public Articles ajouterArticle(Articles article) {
        return articlesRepository.save(article);
    }


    @Override
    public Articles convertirDtoToArticle(ArticlesDto dto) {

        Articles article = new Articles();
        article.setNomArticle(dto.getNomArticle());
        article.setDescription(dto.getDescription());
        article.setPrix(dto.getPrix());
        article.setQuantiteStock(dto.getQuantiteStock());
   article.setImagePrincpale(dto.getImagePrincpale());

   LocalDate date =  LocalDate.now();
   article.setCreatedAt(date);

        // Convert TypeArticle
        if (dto.getTypeArticleId() != null) {
            TypeArticle typeArticle = typeArticleRepository.findById(dto.getTypeArticleId())
                    .orElseThrow(() -> new RuntimeException("TypeArticle not found"));
            article.setTypeArticle(typeArticle);
        }

        // Convert Taille
        if (dto.getTailleIds() != null) {
            List<Taille> tailles = tailleRepository.findAllById(dto.getTailleIds());
            article.setTaille(tailles);
        }

        // Convert Couleur
//        if (dto.getCouleurIds() != null) {
//            Collection<Couleur> couleurs = couleurRepository.findAllById(dto.getCouleurIds());
//            article.setCouleur(couleurs);
//        }

        return article;
    }


    @Override
    public List<Articles> findAllArticle() {
        return articlesRepository.findAll();
    }

    @Override
    public Articles getArticles(Long id) {
        return articlesRepository.findArticlesById(id);
    }

    @Override
    public void deleteArticle(Long id) {
        Articles deleteArticle = articlesRepository.findById(id).orElse(null);
        articlesRepository.deleteById(deleteArticle.getId());
    }

    @Override
    public long getArticlesCount() {
        return articlesRepository.count();
    }

    @Override
    public Articles updateArticle(Articles articles, Long id ) {

        Articles existingArticle = articlesRepository.findById(id).orElse(null);

        existingArticle.setNomArticle(articles.getNomArticle());
        existingArticle.setDescription(articles.getDescription());
        existingArticle.setPrix(articles.getPrix());
        existingArticle.setQuantiteStock(articles.getQuantiteStock());

        if(articles.getTypeArticle() != null && articles.getTypeArticle().getId() != null ){
            TypeArticle typeArticle = typeArticleRepository.findById(articles.getTypeArticle().getId()).orElseThrow(()-> new RuntimeException("TypeArticle not found" + id));
            existingArticle.setTypeArticle(typeArticle);
        }


        if(articles.getImagePrincpale() != null && !articles.getImagePrincpale().isEmpty()){
            existingArticle.setImagePrincpale(articles.getImagePrincpale());
        }

        return articlesRepository.save(existingArticle);
    }

    @Override
    public List<Articles> getArticlesByCategory(Long categoryId) {
        // Récupérer la catégorie
        Optional<Categories> category = categoriesRepository.findById(categoryId);

        // Récupérer les TypeArticle associés à cette catégorie
        List<TypeArticle> typeArticles = typeArticleRepository.findByCategoriesId(categoryId);

        // Récupérer tous les articles associés aux TypeArticle
        List<Articles> articles = new ArrayList<>();
        for (TypeArticle typeArticle : typeArticles) {
            articles.addAll(articlesRepository.findByTypeArticleId(typeArticle.getId()));
        }

        return articles;
    }

    @Override
    public List<Articles> newArticleList(){
        LocalDate yesterday = LocalDate.now().minusDays(2);
        return articlesRepository.findByCreatedAtAfter(yesterday);
    }
}
