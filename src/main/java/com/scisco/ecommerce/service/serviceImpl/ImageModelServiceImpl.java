package com.scisco.ecommerce.service.serviceImpl;

import com.scisco.ecommerce.model.ImageModel;
import com.scisco.ecommerce.repository.ImageModelRepository;
import com.scisco.ecommerce.service.ImageModelService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class ImageModelServiceImpl implements ImageModelService {

    private ImageModelRepository imageModelRepository;

    public ImageModelServiceImpl(ImageModelRepository imageModelRepository) {
        this.imageModelRepository = imageModelRepository;
    }

    @Override
    public Optional<ImageModel> findByName(String nom){
        return imageModelRepository.findByName(nom);
    }

    @Override
    public ImageModel addimage(ImageModel imageModel){
        return imageModelRepository.save(imageModel);
    }
}
