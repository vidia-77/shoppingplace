package com.scisco.ecommerce.service.serviceImpl;

import com.scisco.ecommerce.model.Utilisateur;
import com.scisco.ecommerce.repository.UtilisateurRepository;
import com.scisco.ecommerce.security.Regex;
import com.scisco.ecommerce.service.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private UtilisateurRepository utilisateurRepository;
    private PasswordEncoder passwordEncoder;

    public UserServiceImpl(UtilisateurRepository utilisateurRepository, PasswordEncoder passwordEncoder) {
        this.utilisateurRepository = utilisateurRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Utilisateur saveUser(Utilisateur utilisateur){
        utilisateur.setMotDePasse(passwordEncoder.encode(utilisateur.getMotDePasse()));
        utilisateur.setCreateTime(LocalDateTime.now());

        if(utilisateur.getMail().matches(Regex.regexEmail)){
            System.out.println("ok");
        }

        return utilisateurRepository.save(utilisateur);

    }

    @Override
    public Optional<Utilisateur> findByNom(String nom){ return utilisateurRepository.findByNom(nom);}
/*
    @Override
    public void changeRole(Role newRole, String nom){
        utilisateurRepository.updateUtilisateurRole(nom, newRole);
    }
*/
    @Override
    public List<Utilisateur> findAllUser(){
        return utilisateurRepository.findAll();
    }

    @Override
    public Utilisateur updateUser(Utilisateur user, Long id){

        Utilisateur updateUser = utilisateurRepository.findById(id).orElse(null);


        updateUser.setNom(user.getNom());
        updateUser.setPrenom(user.getPrenom());
        updateUser.setMail(user.getMail());
        updateUser.setDateNaissance(user.getDateNaissance());
        updateUser.setSexe(user.getSexe());
        updateUser.setAdresse1(user.getAdresse1());
        updateUser.setCodePostale(user.getCodePostale());
        updateUser.setVille(user.getVille());
        updateUser.setRaisonSocial(user.getRaisonSocial());
        updateUser.setNumeroTva(user.getNumeroTva());
        updateUser.setSiret(user.getSiret());
        updateUser.setAccessToken(user.getAccessToken());
        updateUser.setRefreshToken(user.getRefreshToken());
        updateUser.setCreateTime(user.getCreateTime());
        updateUser.setRole(user.getRole());


        if(user.getMotDePasse()!=null && !user.getMotDePasse().isEmpty()){
            updateUser.setMotDePasse(user.getMotDePasse());
        }
        return utilisateurRepository.save(updateUser);
    }

    @Override
    public void deleteUtilisateur(Long id) {
        utilisateurRepository.deleteUtilisateurById(id);
    }

    @Override
    public long  getUserRoleCount(){return utilisateurRepository.count();}

}
