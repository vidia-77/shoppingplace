package com.scisco.ecommerce.service.serviceImpl;

import com.scisco.ecommerce.security.jwt.JwtProvider;
import com.scisco.ecommerce.model.Utilisateur;
import com.scisco.ecommerce.security.UserPrincipal;
import com.scisco.ecommerce.service.AuthenticationService;
import com.scisco.ecommerce.service.JwtRefreshTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;


@Service
public class AuthenticationServiceImpl implements AuthenticationService {


    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtProvider jwtProvider;
    @Autowired
    private JwtRefreshTokenService jwtRefreshTokenService;


    @Override
    public Utilisateur signInAndReturnJWT(Utilisateur signInRequest)
    {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(signInRequest.getNom(), signInRequest.getMotDePasse())
        );
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        String jwt = jwtProvider.generateToken(userPrincipal);

        Utilisateur signInUser = userPrincipal.getUtilisateur();
        signInUser.setAccessToken(jwt);
        signInUser.setRefreshToken(jwtRefreshTokenService.createRefreshToken(signInUser.getId()).getTokenId());

        return signInUser;
    }

}
