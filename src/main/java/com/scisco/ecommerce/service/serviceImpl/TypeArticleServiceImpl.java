package com.scisco.ecommerce.service.serviceImpl;

import com.scisco.ecommerce.repository.TypeArticleRepository;
import com.scisco.ecommerce.service.TypeArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TypeArticleServiceImpl implements TypeArticleService {

    @Autowired
    private TypeArticleRepository typeArticleRepository;

    @Override
    public long typeArticleCount() {
        return    typeArticleRepository.count();

    }
}
