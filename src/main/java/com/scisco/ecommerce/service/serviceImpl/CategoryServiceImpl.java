package com.scisco.ecommerce.service.serviceImpl;

import com.scisco.ecommerce.repository.CotegoriesRepository;
import com.scisco.ecommerce.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CotegoriesRepository categoryRepository;


    @Override
    public long getGategoryCount(){
        return categoryRepository.count();
    }



}
