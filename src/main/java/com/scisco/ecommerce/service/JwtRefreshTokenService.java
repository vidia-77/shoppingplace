package com.scisco.ecommerce.service;

import com.scisco.ecommerce.model.JwtRefreshToken;
import com.scisco.ecommerce.model.Utilisateur;

public interface JwtRefreshTokenService {

    JwtRefreshToken createRefreshToken(Long userId);

    Utilisateur generateAccessTokenFromRefreshToken(String refreshTokenId);
}
