package com.scisco.ecommerce.service;

import com.scisco.ecommerce.model.ImageModel;

import java.util.Optional;

public interface ImageModelService {
    Optional<ImageModel> findByName(String nom);

    ImageModel addimage(ImageModel imageModel);
}
