package com.scisco.ecommerce.service;

import com.scisco.ecommerce.dto.ArticlesDto;
import com.scisco.ecommerce.model.Articles;

import java.util.List;

public interface ArticleService {
    Articles ajouterArticle(Articles article);

    Articles convertirDtoToArticle(ArticlesDto article);


    List<Articles> findAllArticle();

    Articles getArticles(Long id);

    void deleteArticle(Long id);

    long getArticlesCount();

    Articles updateArticle(Articles articles, Long id);

    List<Articles> getArticlesByCategory(Long categoryId);

    List<Articles> newArticleList();
}
