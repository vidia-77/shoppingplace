package com.scisco.ecommerce.service;

import com.scisco.ecommerce.model.Utilisateur;

public interface AuthenticationService {

    Utilisateur signInAndReturnJWT(Utilisateur signInRequest);
}
