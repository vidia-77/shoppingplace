package com.scisco.ecommerce.service;

import com.scisco.ecommerce.model.Utilisateur;

import java.util.List;
import java.util.Optional;

public interface UserService {

    Utilisateur saveUser(Utilisateur utilisateur);

    Optional<Utilisateur> findByNom(String nom);

  //  void changeRole(Role newRole, String nom);

    List<Utilisateur> findAllUser();



    Utilisateur updateUser(Utilisateur user, Long id);

    void deleteUtilisateur(Long id);

    long getUserRoleCount();
}
