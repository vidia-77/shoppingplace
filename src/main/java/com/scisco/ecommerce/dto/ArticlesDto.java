package com.scisco.ecommerce.dto;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;

@Getter
@Setter
@AllArgsConstructor

public class ArticlesDto implements Serializable {
    private final Long id;
    private final String nomArticle;
    private final String description;
    private final double prix;
    private final int quantiteStock;
    private final LocalDate createdAt;
    private final MultipartFile imageEnvoyer;
    private final String imagePrincpale;
    private Long typeArticleId;
    private List<Long> tailleIds;
    private List<Long> couleurIds;
}
