package com.scisco.ecommerce.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="LIVRAISON")
public class Livraison implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;
    private String dateLivraison;
    @Embedded
    private Adresse adresse;

    @OneToMany(mappedBy = "livraison")
    private Collection<CommandeClient> commandeClients;

}



