package com.scisco.ecommerce.model;

import lombok.*;


import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="ARTICLES")
public class Articles implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;
    private String nomArticle;
    private String description;
    private double prix;
    private int quantiteStock;
    private String imagePrincpale;
    private LocalDate createdAt;
    @ManyToOne
    @JoinColumn(name = "typeArticle_id")
    private TypeArticle typeArticle;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "articles")
    private Collection<LigneStock> ligneStock;

    @OneToMany(mappedBy = "articles")
    private List<LigneCommandeClient> ligneCommandeClients;


    @ManyToMany
    @JoinTable(name = "articles_taille",
            joinColumns = @JoinColumn(name = "articles_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "taille_id", referencedColumnName = "id"))
    private List<Taille> taille;

    @ManyToMany
    @JoinTable(name = "articles_color", joinColumns = @JoinColumn(name = "articles_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "color_id", referencedColumnName = "id"))
    private Collection<Couleur> couleur;




}
