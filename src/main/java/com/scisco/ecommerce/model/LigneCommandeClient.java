package com.scisco.ecommerce.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="LIGNE_COMMANDE_CLIENT")
public class LigneCommandeClient implements Serializable {

    @EmbeddedId
    private LigneId ligneId;

    private Integer quantite;

    @MapsId("articlesId")
    @ManyToOne
    @JoinColumn(name = "articles_id")
    private Articles articles;

    @MapsId("commandeClientId")
    @ManyToOne
    @JoinColumn(name = "commande_client_id")
    private CommandeClient commandeClient;

    @ManyToOne
    @JoinColumn(name = "taille_id")
    private Taille taille;

    @ManyToOne
    @JoinColumn(name = "color_id")
    private Couleur couleur;

   // @ManyToOne
   // private CommandeClient commandeClient;

   // @ManyToOne
   // private Articles articles;
}
