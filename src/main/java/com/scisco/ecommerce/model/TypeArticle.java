package com.scisco.ecommerce.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name="TYPE_ARTICLE")
public class TypeArticle implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;
    private String nomType;
    private String imageTypeArticle;

    @ManyToOne
    @JoinColumn(name = "categorie_id")
    @JsonIgnore
    private Categories categories;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL,mappedBy = "typeArticle")
    @JsonIgnore
    private Set<Articles> articles;


}
