package com.scisco.ecommerce.model;

import lombok.*;
import javax.persistence.Embeddable;


@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode

// adresse est une class embarqué
@Embeddable
public class Adresse {

    private String adresse1;
    private String ville;
    private Integer CodePostale;

}
