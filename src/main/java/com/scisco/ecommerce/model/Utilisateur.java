package com.scisco.ecommerce.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;



@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "USERS")
public class Utilisateur implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(unique = true, nullable = false, length = 100)
    private String nom;
    private String prenom;
    @Column(name = "mdp", nullable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String motDePasse;
    @Temporal(TemporalType.DATE)
    // temporal supprime la valeur de l'heure et ne conserve que la date .
    private Date dateNaissance;
    private String sexe;
    @Column(unique = true)
    private String mail;
    private String adresse1;
    private String ville;
    private Integer codePostale;
    private String raisonSocial;
    @Column(unique = true)
    private String numeroTva;
    @Column(unique = true)
    private String siret;
    // Transient est utilisé pour marquer une variable comme non sérialisable. Il est utilisé lorsque nous sérialisons un objet.
    @Transient
    private String accessToken;
    @Transient
    private String refreshToken;
    //@Column(nullable = false)
    private LocalDateTime createTime;
    @OneToMany(mappedBy = "utilisateur")
    private Collection<LigneStock> ligneStock;
    @OneToMany(mappedBy = "utilisateur")
    private Collection<CommandeClient> commandeClient;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Role role;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getAdresse1() {
        return adresse1;
    }

    public void setAdresse1(String adresse1) {
        this.adresse1 = adresse1;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public Integer getCodePostale() {
        return codePostale;
    }

    public void setCodePostale(Integer codePostale) {
        this.codePostale = codePostale;
    }

    public String getRaisonSocial() {
        return raisonSocial;
    }

    public void setRaisonSocial(String raisonSocial) {
        this.raisonSocial = raisonSocial;
    }

    public String getNumeroTva() {
        return numeroTva;
    }

    public void setNumeroTva(String numeroTva) {
        this.numeroTva = numeroTva;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public Collection<LigneStock> getLigneStock() {
        return ligneStock;
    }

    public void setLigneStock(Collection<LigneStock> ligneStock) {
        this.ligneStock = ligneStock;
    }

    public Collection<CommandeClient> getCommandeClient() {
        return commandeClient;
    }

    public void setCommandeClient(Collection<CommandeClient> commandeClient) {
        this.commandeClient = commandeClient;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
