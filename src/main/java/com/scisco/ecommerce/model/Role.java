package com.scisco.ecommerce.model;

public enum Role {
    PARTICULIER,
    PROFESSIONNEL,
    ADMIN
}
