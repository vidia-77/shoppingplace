package com.scisco.ecommerce.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;


@Getter
@Setter
@NoArgsConstructor
@Embeddable
public class LigneId implements Serializable {
    @Column(name = "articles_id")
    private long articlesId;
    @Column(name = "commande_client_id")
    private long commandeClientId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LigneId)) return false;

        LigneId ligneId = (LigneId) o;

        if (getArticlesId() != ligneId.getArticlesId()) return false;
        return getCommandeClientId() == ligneId.getCommandeClientId();
    }

    @Override
    public int hashCode() {
        int result = (int) (getArticlesId() ^ (getArticlesId() >>> 32));
        result = 31 * result + (int) (getCommandeClientId() ^ (getCommandeClientId() >>> 32));
        return result;
    }
}
