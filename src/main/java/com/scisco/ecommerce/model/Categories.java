package com.scisco.ecommerce.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name="CATEGORIES")
public class Categories implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;
    private String nomCategories;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categories")
    private Set<TypeArticle> typeArticle;
}
