package com.scisco.ecommerce.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="STOCK")
public class LigneStock implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;
    private Integer prix;
    private Integer quantiteStock;
    private Date dateEntre;
    private Date dateSortie;

    @ManyToOne
    private Utilisateur utilisateur;

    @ManyToOne
    private Articles articles;



}
