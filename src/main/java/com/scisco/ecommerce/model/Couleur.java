package com.scisco.ecommerce.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="COLOR")
public class Couleur implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;
    private String codeCouleur;
/*
    @ManyToOne
    @JoinColumn(name = "articles_id")
    @JsonIgnore
    private Articles article;

*/




}
