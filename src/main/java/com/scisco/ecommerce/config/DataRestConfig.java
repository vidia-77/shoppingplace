package com.scisco.ecommerce.config;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.validation.Validator;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

import javax.persistence.EntityManager;
import javax.persistence.metamodel.EntityType;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;



@Configuration
@AllArgsConstructor
public class DataRestConfig implements RepositoryRestConfigurer {

    private EntityManager entityManager;
    private Validator validator;


     @Autowired
    public DataRestConfig (EntityManager theEntityManager){
        entityManager=theEntityManager;
    }

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config, CorsRegistry cors) {
        RepositoryRestConfigurer.super.configureRepositoryRestConfiguration(config, cors);
        exposeId(config);
    }

   /* @Override
    public void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {
        validatingListener.addValidator("beforeCreate",this.validator);
        validatingListener.addValidator("beforeSave",this.validator);
    }*/

    private void exposeId(RepositoryRestConfiguration config) {

        //xxxxxxxxxx methode pour exposer les id des class xxxxxxxxxxxxxxxxxxx
        // recuperer la liste des entité class depuis entity manager
        Set<EntityType<?>> entites= entityManager.getMetamodel().getEntities();

        // creer une liste des entité class
        List<Class>  entityClases=new ArrayList<>();

        //reccupere le type des entité pour les entité
        for(EntityType entityType : entites){
            entityClases.add(entityType.getJavaType());
        }

        // expose les id de l'entité dans un array des entité
        Class[] domainType = entityClases.toArray(new Class[0]);
        config.exposeIdsFor(domainType);
    }


}
