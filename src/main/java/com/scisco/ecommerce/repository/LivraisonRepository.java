package com.scisco.ecommerce.repository;

import com.scisco.ecommerce.model.Livraison;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = {"https://shoppingplace.netlify.app","http://localhost:4200/"})
@RepositoryRestResource(collectionResourceRel = "LivraisonCommande",itemResourceRel = "livraison", path = "livraison")
public interface LivraisonRepository extends JpaRepository<Livraison, Long> {
}
