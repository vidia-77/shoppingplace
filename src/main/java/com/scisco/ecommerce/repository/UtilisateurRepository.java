package com.scisco.ecommerce.repository;
import com.scisco.ecommerce.model.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;


import java.util.Optional;

@CrossOrigin(origins = {"https://shoppingplace.netlify.app","http://localhost:4200/"})
@Repository
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {


    Optional<Utilisateur>findByNom(String nom);
    //@Modifying
    //@Query("update Utilisateur set role=:role where nom =:nom")
    //void updateUtilisateurRole(@Param("nom") String nom, @Param("role") Role role);
    void deleteUtilisateurById(Long id);
}
