package com.scisco.ecommerce.repository;

import com.scisco.ecommerce.model.Categories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Optional;

@CrossOrigin(origins = {"https://shoppingplace.netlify.app","http://localhost:4200/"})
@RepositoryRestResource(collectionResourceRel = "categorieArticle",itemResourceRel = "categorie", path = "categorie")
public interface CotegoriesRepository extends JpaRepository<Categories, Long> {
    Optional<Categories> findById(Long id);
}
