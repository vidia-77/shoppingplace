package com.scisco.ecommerce.repository;

import com.scisco.ecommerce.model.LigneStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = {"https://shoppingplace.netlify.app","http://localhost:4200/"})
@RepositoryRestResource(collectionResourceRel = "ligneStockArticle",itemResourceRel = "ligneStock", path = "stock")
public interface LigneStockRepository extends JpaRepository<LigneStock , Long> {
}
