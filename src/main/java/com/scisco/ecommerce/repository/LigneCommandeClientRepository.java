package com.scisco.ecommerce.repository;

import com.scisco.ecommerce.model.LigneCommandeClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = {"https://shoppingplace.netlify.app","http://localhost:4200/"})
@RepositoryRestResource(collectionResourceRel = "ligneCommandeClient",itemResourceRel = "ligneCommande", path = "ligneCommande")
public interface LigneCommandeClientRepository extends JpaRepository<LigneCommandeClient, Long> {
}
