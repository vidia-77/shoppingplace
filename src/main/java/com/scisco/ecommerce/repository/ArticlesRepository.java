package com.scisco.ecommerce.repository;

import com.scisco.ecommerce.model.Articles;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.List;


@CrossOrigin(origins = {"https://shoppingplace.netlify.app","http://localhost:4200/"})
@RepositoryRestResource(collectionResourceRel = "listeArticle",itemResourceRel = "article" , path = "article")
public interface ArticlesRepository extends JpaRepository<Articles, Long> {

    List<Articles> findByTypeArticleId( Long id);
    // SELECT * FROM articles WHERE nom_article LIKE CONCAT ('%',nom_article,'%');
    // link : http://localhost:8080/api/article/search/findByNomArticleContaining?nomArticle=saris
    Page<Articles> findByNomArticleContaining(@RequestParam("nomArticle") String nomArticle, Pageable pageable);

    Articles save(Articles articles);
    Articles findArticlesById(Long id);
    void deleteArticlesById(Long id);

    List<Articles> findByCreatedAtAfter(LocalDate date);
}
