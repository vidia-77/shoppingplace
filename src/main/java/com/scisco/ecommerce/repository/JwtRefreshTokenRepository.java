package com.scisco.ecommerce.repository;

import com.scisco.ecommerce.model.JwtRefreshToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JwtRefreshTokenRepository extends JpaRepository<JwtRefreshToken, String > {

}
