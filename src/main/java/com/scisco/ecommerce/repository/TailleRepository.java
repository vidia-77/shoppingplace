package com.scisco.ecommerce.repository;

import com.scisco.ecommerce.model.Taille;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin(origins = {"https://shoppingplace.netlify.app","http://localhost:4200/"})
@RepositoryRestResource(collectionResourceRel = "tailleArticle",itemResourceRel = "taille", path = "taille")
public interface TailleRepository extends JpaRepository<Taille, Long> {
    // Page<Taille> findByArticles_Id(Long articlesId, Pageable pageable);

    Taille findByCodeTaille(String codeTaille);
}
