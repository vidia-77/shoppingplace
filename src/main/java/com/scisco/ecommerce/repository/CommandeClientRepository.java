package com.scisco.ecommerce.repository;

import com.scisco.ecommerce.model.CommandeClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = {"https://shoppingplace.netlify.app","http://localhost:4200/"})
@RepositoryRestResource(collectionResourceRel = "commandeClient",itemResourceRel = "commandeClient", path = "commande")
// commandeClient Name of json entry & commande le chemin d'accée
public interface CommandeClientRepository extends JpaRepository<CommandeClient,Long> {
}
