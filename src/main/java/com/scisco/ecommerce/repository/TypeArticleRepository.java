package com.scisco.ecommerce.repository;

import com.scisco.ecommerce.model.TypeArticle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@CrossOrigin(origins = {"https://shoppingplace.netlify.app","http://localhost:4200/"})
@RepositoryRestResource(collectionResourceRel = "typeArticle", path = "type")
public interface TypeArticleRepository extends JpaRepository<TypeArticle, Long> {

List<TypeArticle> findByCategoriesId(@RequestParam("id") Long id);

}
