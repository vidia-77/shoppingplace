package com.scisco.ecommerce.controller;

import com.scisco.ecommerce.dto.ArticlesDto;
import com.scisco.ecommerce.model.Articles;
import com.scisco.ecommerce.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/article")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @PostMapping(value = "/ajouter", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Articles> addArticle (@ModelAttribute ArticlesDto articlesDto){
        Articles article = articleService.convertirDtoToArticle(articlesDto);
        Articles createArticle = articleService.ajouterArticle(article);
        return new ResponseEntity<>(createArticle, HttpStatus.CREATED);
    }

    @GetMapping("/all")
    public ResponseEntity findAllArticle()
    {
        return ResponseEntity.ok(articleService.findAllArticle());
    }

    @GetMapping(path = "/{id}")
    public Articles getArticle(@PathVariable("id") Long id ){
        return articleService.getArticles(id);
    }

    @DeleteMapping("/supprimer/{id}")
    public ResponseEntity<Articles> deleteArticle(@PathVariable("id") Long id){
        articleService.deleteArticle(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @ResponseBody
    public long countArticles(){
        long count= articleService.getArticlesCount();
        return count;
    }

    @PutMapping("/{id}")
    public ResponseEntity<Articles> updateArticle(@RequestBody ArticlesDto dto, @PathVariable Long id){
        Articles article = articleService.convertirDtoToArticle(dto);
        Articles updateArticle = articleService.updateArticle(article, id);
        return new ResponseEntity<>(updateArticle, HttpStatus.ACCEPTED);
    }

    @GetMapping("/byCategory/{categoryId}")
    public List<Articles> getArticlesByCategory(@PathVariable Long categoryId) {
        return articleService.getArticlesByCategory(categoryId);
    }

    @GetMapping("/new")
    public List<Articles> getNouveauxArticles() {
        return articleService.newArticleList();
    }

}
