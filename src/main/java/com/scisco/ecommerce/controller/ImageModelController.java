package com.scisco.ecommerce.controller;

import com.scisco.ecommerce.model.Articles;
import com.scisco.ecommerce.model.ImageModel;
import com.scisco.ecommerce.service.ImageModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import static java.nio.file.Files.copy;
import static java.nio.file.Paths.get;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;

@RestController
@RequestMapping("/image")
public class ImageModelController {

   @Autowired
   ImageModelService imageModelService;
/*
   @PostMapping("/upload")
   public ResponseEntity.BodyBuilder uplaodImage(@RequestParam("image") MultipartFile file) throws IOException {
      System.out.println("Original Image Byte Size - " + file.getBytes().length);
      return ResponseEntity.status(HttpStatus.OK);
   }
*/


   @PostMapping("/upload")
   public ResponseEntity<List<String>> uploadFiles(@RequestParam("files")List<MultipartFile> multipartFiles) throws IOException {
      List<String> filenames = new ArrayList<>();
      for(MultipartFile file : multipartFiles) {
         String filename = StringUtils.cleanPath(file.getOriginalFilename());
         ImageModel img = new ImageModel(file.getOriginalFilename(), file.getContentType(),
                 compressBytes(file.getBytes()));
         imageModelService.addimage(img);
      }
      return ResponseEntity.ok().body(filenames);
   }

   @GetMapping(path = { "/get/{imageName}" })
   public ImageModel getImage(@PathVariable("imageName") String imageName) throws IOException {
      final Optional<ImageModel> retrievedImage = imageModelService.findByName(imageName);
      ImageModel img = new ImageModel(retrievedImage.get().getName(), retrievedImage.get().getType(),
              decompressBytes(retrievedImage.get().getPicByte()));
      return img;
   }

   // compress the image bytes before storing it in the database
   public static byte[] compressBytes(byte[] data) {
      Deflater deflater = new Deflater();
      deflater.setInput(data);
      deflater.finish();

      ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
      byte[] buffer = new byte[1024];
      while (!deflater.finished()) {
         int count = deflater.deflate(buffer);
         outputStream.write(buffer, 0, count);
      }
      try {
         outputStream.close();
      } catch (IOException e) {
      }
      System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);
      return outputStream.toByteArray();

   }

   // uncompress the image bytes before returning it to the angular application
   public static byte[] decompressBytes(byte[] data) {
      Inflater inflater = new Inflater();
      inflater.setInput(data);
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
      byte[] buffer = new byte[1024];
      try {
         while (!inflater.finished()) {
            int count = inflater.inflate(buffer);
            outputStream.write(buffer, 0, count);
         }
         outputStream.close();
      } catch (IOException ioe) {
      } catch (DataFormatException e) {
      }
      return outputStream.toByteArray();
   }






}
