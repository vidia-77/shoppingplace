package com.scisco.ecommerce.controller;

import com.scisco.ecommerce.service.TypeArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/typeArticle")
public class TypeArticleController {

    @Autowired
    private TypeArticleService typeArticleService;

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @ResponseBody
    public long getTypeArticleCount() {
        long count = typeArticleService.typeArticleCount();
        return count;
    }
}
