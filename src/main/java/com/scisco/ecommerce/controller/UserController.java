package com.scisco.ecommerce.controller;

import com.scisco.ecommerce.model.Articles;
import com.scisco.ecommerce.model.Role;
import com.scisco.ecommerce.model.Utilisateur;
import com.scisco.ecommerce.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")//pre-path
public class UserController
{
    @Autowired
    private UserService userService;

    @GetMapping("/all")
    public ResponseEntity findAllUsers()
    {
        return ResponseEntity.ok(userService.findAllUser());
    }

    @PutMapping("/{id}")
    public ResponseEntity updateUser(@RequestBody Utilisateur user, @PathVariable("id") Long id ){
        return ResponseEntity.ok(userService.updateUser(user, id));
    }

    @DeleteMapping("/supprimer/{id}")
    public ResponseEntity<Utilisateur> deleteUser(@PathVariable("id") Long id){
        userService.deleteUtilisateur(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @ResponseBody
    public long countUser(){
        long count=userService.getUserRoleCount();
        return count;
    }

    @PostMapping("/")
    public ResponseEntity<Utilisateur> createUser(@RequestBody Utilisateur user){
        Utilisateur savedUser = userService.saveUser(user);
        return new ResponseEntity<>(savedUser, HttpStatus.CREATED);
    }



}