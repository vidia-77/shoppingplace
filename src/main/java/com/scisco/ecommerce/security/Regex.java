package com.scisco.ecommerce.security;

public class Regex {

    public static final String regexTel = "^(0[1-68])(?:[ _.-]?(\\d{2})){4}$";

    public static final String regexNom = "^[a-zA-ZÀ-ú ,.'-]+$";

    public static final String regexEmail =  "^([\\w-]+(?:\\.[\\w-]+)*)@((?:[\\w-]+\\.)*\\w[\\w-]{0,66})\\.([a-z]{2,6}(?:\\.[a-z]{2})?)$";

    public static final String regexMdp = "^((?=\\S*?[A-Z])(?=\\S*?[a-z])(?=\\S*?[0-9]).{6,})\\S$";
}

