package com.scisco.ecommerce.security;

import com.scisco.ecommerce.model.Utilisateur;
import com.scisco.ecommerce.security.UserPrincipal;
import com.scisco.ecommerce.service.UserService;
import com.scisco.ecommerce.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Utilisateur utilisateur = userService.findByNom(username) .orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));

        Set<GrantedAuthority> authorities = Set.of(SecurityUtils.convertToAuthority(utilisateur.getRole().name()));

        return UserPrincipal.builder()
                .utilisateur(utilisateur)
                .id(utilisateur.getId())
                .nom(utilisateur.getNom())
                .motDePasse(utilisateur.getMotDePasse())
                .authorities(authorities)
                .build();
    }
}
