INSERT INTO CATEGORIES(id,nom_categories) VALUES (1, "Homme");
INSERT INTO CATEGORIES(id,nom_categories) VALUES (2, "Femme");
INSERT INTO CATEGORIES(id,nom_categories) VALUES (3, "Enfant");
INSERT INTO CATEGORIES(id,nom_categories) VALUES (4, "Bébé");
INSERT INTO CATEGORIES(id,nom_categories) VALUES (5, "Maison");
INSERT INTO CATEGORIES(id,nom_categories) VALUES (6, "High-Tech");


INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (1,"Chemise",1);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (2,"T-shirt",1);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (3,"Pantalon",1);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (4,"Blazer",1);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (5,"Chaussure",1);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (6,"Accesoire",1);

INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (7,"Robe",2);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (8,"Veste",2);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (9,"Pull",2);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (10,"Chaussure",2);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (11,"Jupe",2);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (12,"Leggings",2);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (13,"Accesoire",2);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (14,"Sous-Vetement",2);

INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (15,"body",3);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (16,"Pyjama",3);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (17,"Ensemble",3);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (18,"Chaussure",3);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (19,"Jouet",3);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (20,"Pull",3);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (21,"Pantalon",3);

INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (22,"Ensemble",4);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (23,"body",4);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (24,"Pyjama",4);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (25,"gigoteuse",4);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (26,"Jouets",4);

INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (27,"Decoration",5);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (28,"Rideaux",5);

INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (29,"SmartPhone",6);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (30,"Montre-Connecté",6);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (31,"Montre",6);
INSERT INTO TYPE_ARTICLE(id,nom_type,categories_id) VALUES (32,"Ordinateur",6);

INSERT INTO ARTICLES(id,description,nom_article,prix,type_article_id) values (1,"manche-court","chemise-jack-jones",15,1);
INSERT INTO ARTICLES(id,description,nom_article,prix,type_article_id) values (2,"Instructions de lavage: Dry Wash Coste pour Gown seulement","Robe corsée de couleur",20,7);
INSERT INTO ARTICLES(id,description,nom_article,prix,type_article_id) values (3,"6 animaux flottants ideal pour jouer sur le ventre","Tapis d'eau",10,26);

INSERT INTO ROLE_USER(id,nom_role) VALUES (1,"Particulier");
INSERT INTO ROLE_USER(id,nom_role) VALUES (2,"Professionnel");

INSERT INTO USERS (id,code_postale,adresse1,adresse2,ville,date_naissance,mail,mdp,nom,tel,prenom,raison_Social,siret,tva,role_id)
VALUES (1,94000,"30 rue de paris",null,"paris","1959-10-31","toto@gmail.com","1234fr","raja",0605040302,"vidje",null,null,null,1)


