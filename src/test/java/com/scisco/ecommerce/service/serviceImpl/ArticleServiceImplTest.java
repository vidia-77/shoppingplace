package com.scisco.ecommerce.service.serviceImpl;

import com.scisco.ecommerce.model.Articles;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class ArticleServiceImplTest {

    @Autowired
    ArticleServiceImpl articleService;
    @Test
    @Transactional
    void ajouterArticle() {
        Articles article=new Articles();
        article.setNomArticle("chaussure");
        article.setDescription("chaussure mochi");
        article.setPrix(12);
        article.setQuantiteStock(2);
        Articles newArticle =articleService.ajouterArticle(article);
        assertThat(newArticle).isNotNull();
    }
}